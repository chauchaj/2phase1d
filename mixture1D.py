#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 11:05:55 2018

@author: chauchat
"""
###############################################################################
#
# Import librairies
#
###############################################################################
import numpy as np
import scipy
import pylab as pylab

###############################################################################
#
# Numerical parameters
#
###############################################################################
dt=1e-2
tend=1800
kplot=60/dt
#
h=0.1
N=51
dz=h/(float(N-1))

# Mesh definition
# scalar points
zs=np.linspace(0,h,num=N)
zs=zs/h
# Velocity points
zw=np.linspace(-dz/2,h+dz/2,num=N+1)
zw=zw/h

zw[0]=0
zw[N]=1

# Internal nodes list
wNodes = range(N-1)
wNodes = [x+1 for x in wNodes]

scalarNodes = range(N-2)
scalarNodes = [x+1 for x in scalarNodes]
###############################################################################
#
# Physical parameters (Pham Van Bang et al. 2006)
#
###############################################################################
dp=290e-6
rhop=1005e0

rhof = 995e0
nuf = 20e-3/rhof

g = 9.81e0

phiSmall=1e-6
phiMax=0.6

# Stokes response time and settling velocity
tp=rhop*dp**2/(18.*nuf*rhof)
wstokes = (rhop-rhof)*g*dp**2/(18.*rhof*nuf)
print('tpStokes=',tp,'Wstokes=',wstokes)
# Richardson and Zaki exponent
RZ = -1.5

Wmax = 1.1

###############################################################################
##
## Declaration of variables
##
###############################################################################
# Volume fraction
phin = np.zeros(N)
phinp = np.zeros(N)
phiw = np.zeros(N+1)
rhow = np.zeros(N+1)
rho = np.zeros(N)

# Pressure 
pfnp = np.zeros(N)
pp   = np.zeros(N)

dpfdz = np.zeros(N+1)
dppdz = np.zeros(N+1)

# Velocities
wmn = np.zeros(N+1)
wrn = np.zeros(N+1)
wmst = np.zeros(N+1)
wrst = np.zeros(N+1)
wmnp = np.zeros(N+1)
wrnp = np.zeros(N+1)

# Matrices and RHS
# Volume fraction
Aa = np.zeros((N,N))
Ha = np.zeros(N)
# Velocities
Am = np.zeros((N+1,N+1))
Ar = np.zeros((N+1,N+1))
Aminv = np.zeros((N+1,N+1))
Arinv = np.zeros((N+1,N+1))
Ap_f = np.zeros((N+1,N+1))
Hm = np.zeros(N+1)
Hr = np.zeros(N+1)
# Pressure
Apf = np.zeros((N,N))
Hpf = np.zeros(N)

###############################################################################
#
# divergence & gradient operator in matrix form
#
###############################################################################
div  = np.zeros((N,N+1))
grad = np.zeros((N+1,N))

# gradient operator
for j in wNodes:
    grad[j,j-1] = - 1/dz
    grad[j,j  ] =   1/dz   
# Bottom boundary condition
j=0
grad[j,j  ] = -1/dz
grad[j,j+1] =  1/dz

# Top boundary condition
j=N
grad[j,j-2] = -1/dz
grad[j,j-1] =  1/dz

# Divergence operator
for j in wNodes:
    div[j][j  ] = - 1/dz
    div[j][j+1] =   1/dz   
# Bottom boundary condition
j=0
div[j][j  ] = -1/(0.5*dz)
div[j][j+1] =  1/(0.5*dz)
###############################################################################
#
# Initial condition
#
###############################################################################

phi0 = 0.5
h0 = 0.5*h
phin = phi0*0.5*(1+np.tanh((h0/h-zs)*300))

wmn[:] = 0.
# Interpolation of phi at W points
for j in scalarNodes:
    phiw[j] = 0.5*(phin[j]+phin[j-1]) 
phiw[0] = phin[0]
phiw[N] = phin[N-1]
wrn[:] = - wstokes/10.
rhow[:] = phiw[:]*rhop + (1-phiw[:])*rhof
rho[:] = phin[:]*rhop + (1-phin[:])*rhof

time = 0
k=0
###############################################################################
#
#  Temporal loop
#
###############################################################################
while (time<tend):
        time = time + dt
        k = k + 1
        ########################################################
        #
        #        Mass conservation equation
        #
        ########################################################
        for j in scalarNodes:
            FS = dt *  ( wmn[j  ] + (1-phiw[j  ])*wrn[j  ] ) / dz
            FN = dt *  ( wmn[j+1] + (1-phiw[j+1])*wrn[j+1] ) / dz
            #
            Aa[j][j-1] = - max( FS,0)
            Aa[j][j+1] = - max(-FN,0)
            Aa[j][j] = 1e0 -Aa[j][j+1] -Aa[j][j-1] + FN - FS 
            Ha[j] = phin[j]

        # Bottom boundary condition: Neumann
        j=0
        FN = dt *  ( wmn[j+1] + (1-phiw[j+1])*wrn[j+1] )  / (0.5*dz)
        #
        Aa[j][j+1] = - max(-FN,0)
        Aa[j][j] = 1e0 -Aa[j][j+1] + FN 
        Ha[j] = phin[j]
            
        # Top boundary condition: Dirichlet
        Aa[N-1][N-1]=1e0
        Ha[N-1]=0e0

        # Mass conservation resolution
        phinp = np.linalg.solve(Aa, Ha)
        
        ########################################################
        #
        # Interpolation of phi at velocity points
        #
        ########################################################
        
        for j in wNodes:
            phiw[j] = 0.5*(phinp[j]+phinp[j-1]) 
        phiw[0] = phinp[0]
        phiw[N] = phinp[N-1]
        
        rhow[:] = phiw[:]*rhop + (1-phiw[:])*rhof
        rho[:] = phinp[:]*rhop + (1-phinp[:])*rhof
        
        # Compute the particle pressure
        for j in range(N):
            pp[j] = 5e-2 * (max(phinp[j]-0.57,0))**3  \
                          /(max(0.625-phinp[j],phiSmall))**5
        
        # Compute the vertical gradient of particle pressure
        dppdz = grad.dot(pp)
        
        ########################################################
        #
        # Velocity predictor 
        #
        ########################################################
        #
        # Momentum conservation equations: Matrices for Wm and Wr 
        for j in wNodes:
            # Mixture phase matrix  
            if j==1:
                FS = dt *  wmn[j-1] / dz
            else:
                FS = dt *  0.5*(wmn[j  ]+wmn[j-1]) / dz
            if j==N-1:
                FN = dt *  wmn[j+1] / dz
            else:
                FN = dt *  0.5*(wmn[j+1]+wmn[j  ]) / dz
            #
            Am[j][j-1] = - max( FS,0) 
            Am[j][j+1] = - max(-FN,0)
            Am[j][j] = 1e0 - Am[j][j-1] - Am[j][j+1] + FN - FS 
            
            Hm[j] = wmn[j] - dt*dppdz[j]/rhow[j] - dt*phiw[j]*g*(rhop-rhof)/rhow[j]

        # Bottom boundary condition: impermeable wall
        #Particle phase
        Am[0,0]   = 1e0
        Hm[0]     = 0e0
        
        # Top boundary condition: momentum equation
        j=N
        Am[j,j-1] = -1
        Am[j,j  ] =  1

        Hm[j] = 0

        # Solid and fluid phase momentum conservation resolution
        Aminv = np.linalg.inv(Am)
        wmst = Aminv.dot(Hm)
        
        ########################################################
        #                
        # Poisson pressure equation
        #
        ########################################################
        Apf = -dz*div.dot(Am.dot(grad))
        Hpf = -dz*div.dot(wmst)/dt
            
        # Bottom boundary condition: zero gradient (not ideal)
        j = 0

        Apf[j,:] = 0
        Apf[j,j+1] = 1./dz
        Apf[j,j  ] = -1./dz 
        Hpf[j] =  wmst[1]/(dt*Am[1,1])  #(phiw[1]*rhop+(1-phiw[1])*rhof)*g
        
        # Top boundary condition: Dirichlet pf=0
        j = N-1
        Apf[j,:] = 0
        Apf[j,j] = 1
        Hpf[j] = 0
    
        # resolve poisson equation matrix
        pfnp = np.linalg.solve(Apf, Hpf)#*rho

        # Compute fluid phase pressure gradient
        dpfdz = grad.dot(pfnp)
        
        ########################################################
        #
        # Velocity correction
        #
        ########################################################
        
        wmnp = wmst - dt *  Aminv.dot(dpfdz) #/ rhow
        
        # Bottom boundary condition: impermeable wall
        wmnp[0]=0e0
        wmnp[N]=wmnp[N-1]
               
        ########################################################
        #
        # Relative velocity equation
        #
        ########################################################    
        for j in wNodes:
            # relative velocity  matrix
            if j==1:
                FS = dt *  wrn[j-1] / dz
            else:
                 FS = dt * 0.5*(wrn[j  ]+wrn[j-1]) / dz
            if j==N-1:
                FN = dt *  wrn[j+1] / dz
            else:
                FN = dt *  0.5*(wrn[j+1]+wrn[j  ]) / dz
            #
            Ar[j][j-1] = - max( FS,0) 
            Ar[j][j+1] = - max(-FN,0)
            Ar[j][j] = 1e0 - Ar[j][j-1] - Ar[j][j+1] + FN - FS \
                       + dt*(1+rhop*phiw[j]/(rhof*(1.-phiw[j])*tp))*(1-phiw[j])**RZ 
            Hr[j] = wrn[j]  -dt*(1.-rhof/rhop)*g -dt*dppdz[j]/(rhop*(phiw[j]+phiSmall)) 
        # Fluid phase
        # Bottom BC
        Ar[0,0]   = 1e0
        Hr[0]     = 0e0
        # Top BC
        j=N
        Ar[j,j-1] = -1
        Ar[j,j  ] =  1
        Hr[j] = 0
        Arinv = np.linalg.inv(Ar)
        wrnp = Arinv.dot(Hr)
        
        # Ouda and Toorman model
        #wrnp[:] = -wstokes*(1-phiw[:])**(-RZ) 
        #wrnp[-dppdz>=0.999*phiMax*(rhop-rhof)*g] = 0
        
        ########################################################
        #
        # Check the resulting velocity fields are divergence free 
        #
        ########################################################
        Continuity = (np.sum(div.dot(wmnp)**2))**0.5/float(N)
        
        if (Continuity > 1e-4):
            print('Continuity error is too high:',Continuity)

        ########################################################
        #
        #  Plot graphs at run time
        #
        ########################################################
        if (k==kplot):
            print("time=",round(time,5),'s - max(phi)=',round(np.max(phinp),4),' - max(CFL)=',np.max(np.abs(wrnp))*dt/dz)
            #plot
            pylab.figure(1,figsize=(12,8))
            pylab.subplot(141)
            pylab.plot(phinp,zs,'-r')
            #pylab.plot(phiw,zw,':k')
            pylab.axis([0, phiMax, 0, 1])
            pylab.ylabel(r'$z/h$', fontsize=14)
            pylab.xlabel(r'$\phi$', fontsize=12)
             
            pylab.subplot(142)
            pylab.plot(wmnp/wstokes,zw,'-r')
            #pylab.plot(wpst,z,':r')
            pylab.plot(wrnp/wstokes,zw,'-b')
            #pylab.plot(wfst,z,':b')
            pylab.axis([-Wmax, Wmax, 0, 1])
            pylab.xlabel(r'$\frac{W}{Wstokes}$',\
                         fontsize=14)
            pylab.yticks([])
             
            pylab.subplot(143)
            pylab.plot(rho*pfnp/(phiMax*(rhop-rhof)*g*h0),zs,'-b')
            pylab.plot(pp/(phiMax*(rhop-rhof)*g*h0),zs,'-r')
            pylab.axis([0, 1.1, 0, 1])
            pylab.xlabel(r'$\frac{pf}{\phi_m (\rho^p-\rho^f) g h}$', \
                         fontsize=14)
            pylab.yticks([])

             
            pylab.subplot(144)
            pylab.plot(dpfdz/(phiMax*(rhop-rhof)*g),zw,'-b')
            pylab.plot(dppdz/(phiMax*(rhop-rhof)*g),zw,'--r')
            pylab.plot([-1,-1],[0, 1],':k')
            pylab.plot([-phi0/phiMax,-phi0/phiMax],[0, 1],':r')
            pylab.axis([-1.05, 0, 0, 1])
            pylab.xlabel(r'$\frac{d pf/d z}{\phi_m (\rho^p-\rho^f) g}$ ', \
                         fontsize=14)
            pylab.yticks([])

            pylab.show()
            # reset counter for updating plot
            k=0         
        ########################################################
        #    
        #     Update variables for next time step
        #
        ########################################################
        wmn=wmnp
        wrn=wrnp
        phin=phinp
        