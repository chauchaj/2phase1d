\documentclass[11pt]{article}

\usepackage[a4paper,hmargin=2cm,vmargin=2cm]{geometry}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{natbib}
\bibliographystyle{agufull04}

\newcommand{\dd}[2]{\displaystyle\frac{\partial#1}{\partial#2}}
\newcommand{\moy}[1]{\left\langle #1 \right\rangle}
\newcommand{\vecnum}[1]{\left\{#1\right\}}
\newcommand{\matnum}[1]{\left[#1\right]}

\title{1D two-phase flow model PISO algorithm}
\author{J. Chauchat}
%\date{}


\begin{document}

\maketitle


\section{1D two-phase flow equations}

\begin{equation}
\dd{\epsilon}{t} + \dd{\epsilon w^f}{z} = 0
\label{eq:FluidPhaseMass}
\end{equation}
\begin{equation}
\dfrac{\partial {\phi}}{\partial {t}} + \dfrac{\partial{{\phi}w^p}}{\partial{z}}=0,
\label{eq:SolidPhaseMass}
\end{equation}
\begin{equation}
\displaystyle \rho^f \left[\dd{\epsilon w^f}{t}  +  \dd{\epsilon w^f w^f }{z}\right] = - \epsilon \dd{ p^f}{z}  - \dfrac{\rho^p \phi}{t_p} \left(w^f -w^p\right) - \epsilon \rho^f g
\label{eq:FluidPhaseMom}
\end{equation}
\begin{equation}
{\rho^p}\left[\dfrac{\partial \phi w^p}{\partial {t}} + \dfrac{\partial{ \phi w^p w^p}}{\partial{z}}\right]= -  \dd{ p^p}{z}  - \phi \dd{ p^f}{z}  + \dfrac{\rho^p \phi}{t_p} \left(w^f -w^p\right)   - \phi \rho^p g
\label{eq:SolidPhaseMom}
\end{equation}

\noindent where $\phi$ and $\epsilon$ are the volume fractions of solid and fluid phases respectively, $w^p$ and $w^f$ are the vertical velocities of solid and fluid phases respectively, $p^p$ and $p^f$ are the solid and fluid phases pressures respectively, $t_p$ is the particle response time which in the simplest case of a low inertia reduces to the Stokes expression:
\begin{equation}
t_p=\dfrac{\rho^p \ d_p^2 }{ 18 \ \eta^f}  (1-\phi)^{n_{RZ}-2}.
\label{eq:tpStokes}
\end{equation}
including an hindrance function from \cite{richardson1954} with an exponent $n_{RZ}$ typically equal to 5 for very low particulate Reynolds numbers.

The particle pressure $p^p$ represents particle-particle interactions arising from contact forces. This terms should vanish when there is no contact between the particles and start to develop when chain forces between the particles starts to develop. It is well know in granular media that such a network of contacts develop when the volume fraction exceeds the random loose packing fraction ($\phi_{rlp} = 0.57$ for spheres). In an average sense, the particle pressure represents the resistance to compression of the granular media. When the particles are deposited at the bed, it is expected that the particle pressure supports the weight of the particles. 

As we are in a Eulerian description we do not have access to the particle-particle interactions explicitly and we need to model these contact interactions using an empirical model that can only depends on the local volume fraction or the velocities \citep{johnson1987}:
\begin{equation}
\displaystyle 
p^p= \left \lbrace
\begin{array}{ll}
P_0 \ \dfrac{\left(\phi - \phi_{rlp}\right)^3}{\left(\phi_{max} - \phi\right)^5}  & \text{for} \; \; \;  \phi\ge \phi_{rlp}\\
0 &   \text{Otherwise}
\end{array}
\right.
\label{eq:ppPressure}
\end{equation}
 In this model, the particle pressure is a diverging function of $\phi$ at the close random packing fraction $\phi_{max}$ which for sphere is equal to 0.625. This model is not very satisfactory as it is purely empirical but in the mean time there is no other easy options to introduce an elastic stress in the Eulerian vision adopted in the proposed approach. 
 
  \subsection{Fluid-particle velocity formulation}

 The fluid phase continuity equation is not solved, instead the mixture continuity  equation is used. It is obtained by summing the two continuity equation for the fluid and the particle phases (Eqs. \ref{eq:FluidPhaseMass}-\ref{eq:SolidPhaseMass}):
\begin{eqnarray}
\displaystyle \dd{\epsilon+\phi}{t} + \dd{\epsilon w^f + \phi  w^p}{z} = 0 \\
\displaystyle \Leftrightarrow  \dd{\epsilon w^f + \phi  w^p}{z} = 0
\end{eqnarray}
because $\epsilon+\phi=1$ and by defining the volume averaged velocity: $ w^m  = \epsilon w^f+ \phi  w^p$, we obtain that the mixture is incompressible. This result is shown in 1D but it is also true in 3D, therefore we can use classical algorithm developed for incompressible Navier-Stokes equations. 
Also, a classical trick consists in introducing the reduced fluid pressure: $\overline{p^f} = p^f -\rho^f g z$. These to choices lead to the following set of equations:
 \begin{equation}
 \dd{w^m}{z} = 0
\label{eq:FluidPhaseMass}
\end{equation}
\begin{equation}
\dfrac{\partial {\phi}}{\partial {t}} + \dfrac{\partial{{\phi}w^p}}{\partial{z}}=0,
\label{eq:SolidPhaseMass}
\end{equation}
\begin{equation}
\displaystyle \dd{w^f}{t}  +  w^f \dd{w^f }{z}= -  \dfrac{1}{\rho^f}  \dd{ \overline{p^f}}{z}  - \dfrac{\rho^p \phi}{\rho^f \epsilon \ t_p} \left(w^f -w^p\right) 
\label{eq:FluidPhaseMom}
\end{equation}
\begin{equation}
\dfrac{\partial w^p}{\partial {t}} + w^p \dfrac{\partial{w^p}}{\partial{z}} = - \dfrac{1}{\rho^p \phi} \dd{ p^p}{z}  - \dfrac{1}{\rho^p} \dd{ \overline{p^f}}{z}  + \dfrac{1}{t_p} \left(w^f -w^p\right)   - \left(1- \dfrac{\rho^f}{\rho^p} \right) g
\label{eq:SolidPhaseMom}
\end{equation}
 
 \subsection{Mixture-relative velocity formulation}
 
 We derive a mixture model by introducing the mixture velocity $w^m$ and the relative velocity $w_r=w^p-w^f$. The governing equations for each variable is derived by taking the summation and the difference of Eqs (\ref{eq:FluidPhaseMom}-\ref{eq:SolidPhaseMom}):
 
  \begin{equation}
 \dd{w^m}{z} = 0
\label{eq:FluidPhaseMass}
\end{equation}
\begin{equation}
\dfrac{\partial {\phi}}{\partial {t}} + \dfrac{\partial{{\phi}w^p}}{\partial{z}}=0,
\label{eq:SolidPhaseMass}
\end{equation}
\begin{equation}
\displaystyle \dd{\rho w^m}{t}  +  \rho w^m \dd{w^m }{z}= -  \dd{ \overline{p^f}}{z} -  \dd{ p^p}{z}  
\label{eq:MixtureMom}
\end{equation}
\begin{equation}
\dfrac{\partial w^r}{\partial {t}} + w^r \dfrac{\partial{w^r}}{\partial{z}} = - \dfrac{1}{\rho^p \phi} \dd{ p^p}{z}  +\left[ \dfrac{1}{\rho^f} - \dfrac{1}{\rho^p} \right] \dd{ \overline{p^f}}{z}  - \dfrac{1}{t_p} \left[ 1+\dfrac{\rho^p \phi}{\rho^f \epsilon }\right] w^r    - \left(1- \dfrac{\rho^f}{\rho^p} \right) g
\label{eq:SolidPhaseMom}
\end{equation}

 
 \section{PISO algorithm}

In this document the PISO (Pressure Implicit with Splitting of Operators) algorithm for  solving the system of partial differential equations Eqs. (\ref{eq:FluidPhaseMass}-\ref{eq:SolidPhaseMom})  is the fact that, for an incompressible fluid, there is no equation of state for the pressure. In order to predict the fluid pressure in a numerical model one need to use a  pressure-velocity coupling algorithm.


In order to illustrate this point, we will consider a staggered arrangement for the velocities and the fluid pressure, the velocities are face centered $\vecnum{w^f}_j$ and $\vecnum{w^p}_j$ while the scalar quantities such as volume fraction and pressures are located at the cell centers $\vecnum{\phi}_j$. and $\vecnum{p^f}_j$.  For sake of simplicity we will also consider a uniform grid having a constant grid size $\Delta z$. The discretization of the problem will be performed using the finite volume method even though for a one-dimensional problem the result of the discretization is identical to the finite difference method. 
\begin{figure}[htbp]
\begin{center}
\includegraphics[width=5cm]{Fig/Maillage1D.png}\includegraphics[width=5cm]{Fig/Maillage1D_CVscalar.png}\includegraphics[width=5cm]{Fig/Maillage1D_CVw.png}
\caption{Uniform grid with staggered arrangement for the velocities and the scalar quantities such as volume fraction and fluid pressure (left panel). The definition of the control volumes for a scalar quantity and a velocity variable are shown in the middle and right panels respectively.}
\label{fig:1DMesh}
\end{center}
\end{figure}

The complete two-phase flow algorithm is based on the following steps:
\begin{enumerate}
\item Solve for $\vecnum{\phi}_j^{n+1}$ using $\dd{\phi}{t} + \dd{\phi  w^p}{z} = 0$
\item Solve for $\vecnum{\epsilon}_j^{n+1}$ using $\epsilon=1-\phi$
\item Solve for intermediate velocities $\vecnum{w^f}_j^{*}$ and $\vecnum{w^p}_j^{*}$
\item Solve for the pressure $\vecnum{p^f}_j^{*}$ using the poisson equation 
\item Correct the velocities $\vecnum{w^f}_j^{n+1}$ and $\vecnum{w^p}_j^{n+1}$ using the new pressure
\end{enumerate}

We start with the discretization of the solid phase equation Eq. (\ref{eq:SolidPhaseMom}):
\begin{equation}
\int_{t^{n-1}}^{t^n} \int_{z_j}^{z_{j+1}} \left[ \dfrac{\partial {\phi}}{\partial {t}} + \dfrac{\partial{{\phi} w^p}}{\partial{z}} \right] dz dt=0,
\label{eq:SolidPhaseMassFVM1}
\end{equation}
\begin{equation}
 \left(\phi_j^n - \phi_j^{n-1}\right) \Delta z +  \left(w^p \phi \big\vert_N - w^p \phi \big\vert_S\right) \Delta t=0,
\label{eq:SolidPhaseMassFVM2}
\end{equation}
The terms $w^p \phi \big\vert_N$  and $w^p \phi \big\vert_S$ represents the convection fluxes at the North and South interfaces of the scalar control volume (CVs) as shown in the middle panel of figure \ref{fig:1DMesh}. Following classical finite volume schemes \citep[e.g.][]{pathankar1980}, the simple first order upwind convection scheme can be used which allowed to write the discretized the solid phase mass conservation equation:
\begin{equation}
a_S^\phi \phi_{j-1}^n + a_P^\phi \phi_j^n +a_N^\phi \phi_{j+1}^{n} = H^\phi,
\label{eq:SolidPhaseMassFVM3}
\end{equation}
We define the convection fluxes as:
$$F_S =   w_{j}^p  \Delta t  / \Delta z \; \; ; \; \; F_N =  w_{j+1}^p \Delta t    / \Delta z$$
and the first order upwind scheme can be written as:
\begin{eqnarray}
a_S^\phi = - \max( F_S,0)  \\
a_N^\phi = - \max(-F_N,0) \nonumber \\
a_P^\phi = -(a_S^\phi+a_N^\phi) \nonumber \\
\label{eq:MatriceCoefs}
\end{eqnarray}
and the source term is given by $H^\phi = \phi_j^{n-1}$. The boundary conditions needs to be particularized as:
$$\frac{d \phi }{d z } = 0  \; \; ; \; \; a_S^\phi = -1  \; \; ; \; \; a_P^\phi = 1  \; \; ; \; \; H^\phi = 0$$
at the top boundary (j=N-1) and as:
$$F_S =   0 \; \; ; \; \; F_N =  w_{1}^p \Delta t    / (\Delta z/2) \; \; ; \; \; H^\phi = \phi^{n-1}_0 $$
at the bottom boundary (j=0).

Equation (\ref{eq:SolidPhaseMassFVM3}) is a tri-diagonal matrix that can be easily inverted using a double sweep algorithm for example  \citep{thomas1995}. The solution of this algebraic system provide the solution vector  $\vecnum{\phi}_j^{n}$.
 
   

There are different methods for solving the pressure-velocity coupling, they are almost all based on predictor-corrector algorithm. In the following the PISO (Pressure Implicit with Splitting of Operators) algorithm \cite{issa1986} is detailed. This is a very popular algorithm in modern CFD codes and in particular it is implemented in openFOAM. 

The PISO (Pressure Implicit with Splitting of Operators) algorithm can be written as: 
$$\displaystyle \dd{w^p }{t}  +  w^p \dd{w^p}{z} =  -\dfrac{1}{\rho^p \phi}  \dd{p^p}{z}-\dfrac{1}{\rho^p}  \dd{p^f}{z} -  g -  \dfrac{1}{t_p}   \ \left(w^f-w^p\right) \nonumber$$

The semi-discrete form of the equation can be written in matrix form as:
$$ \matnum{A^p}_{jk} \vecnum{w^p}^*_k =  \vecnum{H^p}_j  - \matnum{A^{pf}}_{jk} \vecnum{w^f}^*_k - \dfrac{1}{\rho^p}  \dd{\vecnum{p_f}^*_j}{z}$$
where $ \matnum{A^p}$ contains implicit advection and drag terms, $\vecnum{H^p}$ contains explicit source terms including temporal derivative, gravity, explicit drag term (solid phase contribution), $\matnum{A^{pf}}$ contains the explicit drag term coming from the fluid phase and the index $j$ represents the $j^{st}$ grid node in the mesh.

For example, using a first order Euler scheme for the time derivative, the vector $\vecnum{H^p}_j$ can be written as:
$$\vecnum{H^p}_j =  \vecnum{w^p}^{n-1}_j  -\dfrac{\Delta t}{\rho^p \phi^w_j}  \dfrac{p^p_{j} - p^p_{j-1}}{\Delta z} -  \Delta t \left(1- \dfrac{\rho^f}{\rho^p} \right) g$$
where $\Delta t$ the time step. 

The coupling matrix is diagonal and is written as:
$$\matnum{A^{pf}}_{jj} = -  \dfrac{\Delta t }{t_p}, $$

Using a first order Upwind scheme, the matrix  $\matnum{A^p}$ is tridiagonal and the coefficients are given by:
\begin{equation}
a_S^p \{w^p_{j-1}\}^n + a_P^p \{w^p_j\}^n +a_N^p \{w_{j+1}\}^{n} = H^p,
\label{eq:SolidPhaseMassFVM3}
\end{equation}
We define the convection fluxes as:
$$F_S =   0.5 ( \{w^p\}_{j}^{n-1}+\{w^p\}_{j-1}^{n-1})  \Delta t  / \Delta z \; \; ; \; \; F_N =  0.5 ( \{w^p\}_{j}^{n-1}+\{w^p\}_{j+1}^{n-1})  \Delta t    / \Delta z$$
and the first order upwind scheme can be written as:
\begin{eqnarray}
a_S^p = - \max( F_S,0)  \\
a_N^p = - \max(-F_N,0) \nonumber \\
a_P^p = -(a_S^p+a_N^p) \nonumber \\
\label{eq:MatriceCoefs}
\end{eqnarray}
The boundary conditions needs to be particularized as:
$$\dfrac{d w^p}{d z} = 0  \; \; ; \; \; a_S^p = -1  \; \; ; \; \; a_P^p = 1  \; \; ; \; \; H^p= 0$$
at the top boundary (j=N) and as:
$$w^p = 0  \; \; ; \; \; a_P^p = 1  \; \; ; \; \; H^p= 0 $$
at the bottom boundary (j=0).\\



\noindent \textbf{1. Velocity predictor step}\\

Using the discretized momentum equation, the predictor step for the fluid phase can be written formally as:
$$  \vecnum{w^p}^*= \matnum{A^p}^{-1} \vecnum{H^p} -\matnum{A^{pf}} \vecnum{w^f}^{n-1}$$

This step requires the inversion of the matrix $\matnum{A^p}$.

Similarly, the fluid phase momentum equation in matrix form can be written as:
$$  \vecnum{w^f}^*= \matnum{A^f}^{-1} \vecnum{H^f} -\matnum{A^{fp}} \vecnum{w^p}^{n-1}$$
where the term $\vecnum{H^f}$ is given by:
$$\vecnum{H^f}_j =  \vecnum{w^f}^{n-1}_j $$
and the coupling matrix is given by:
$$\matnum{A^{fp}} = -  \dfrac{\Delta t \rho^p \phi^w}{\rho^f \epsilon^w t_p}   $$

The velocity correction equations integrate the fluid pressure gradient correction and provide the corrected velocity fields  $\vecnum{w^f}^{**}$ and  $\vecnum{w^p}^{**}$:
$$  \vecnum{w^f}^{**}= \vecnum{w^f}^{*} - \dfrac{\Delta t}{\rho^f} \matnum{A^f}^{-1} \dd{\vecnum{p_f}^*_j}{z}$$
$$  \vecnum{w^p}^{**} = \vecnum{w^p}^{*}  - \dfrac{\Delta t}{\rho^p} \matnum{A^p}^{-1} \dd{\vecnum{p_f}^*_j}{z}$$


\noindent \textbf{2. Pressure solution}\\

The corrected velocity fields should be divergence-free for the volume-averaged mixture velocity: $  \vecnum{w_m}^{**}_j =  \vecnum{\epsilon^w}^{n}_j \vecnum{w^f}^{**}_j + \vecnum{\phi^w}^{n}_j \vecnum{w^p}^{**}_j$

$$\dd{ \vecnum{w_m}^{**} }{z}  =0$$

$$\Leftrightarrow \dd{}{z} \left(  \vecnum{\epsilon^w}^{n} \vecnum{w^f}^{**} + \vecnum{\phi^w}^{n} \vecnum{w^p}^{**} \right) =0 $$

$$\Leftrightarrow \dd{}{z} \left[\left(   \dfrac{\vecnum{\epsilon^w}^{n+1} \Delta t}{\rho^f } \matnum{A^f}^{-1} + \dfrac{\vecnum{\phi^w}^{n+1}  \Delta t}{\rho^p }\matnum{A^p}^{-1} \right) \dd{\vecnum{p_f}^*}{z} \right]  = \dd{}{z} \left(  \vecnum{\epsilon^w}^{n+1} \vecnum{w^f}^{*}  + \vecnum{\phi^w}^{n+1}\vecnum{w^p}^{*} \right) $$

The finite volume formulation of Poisson equation gives:
\begin{eqnarray*}
\Leftrightarrow   \left(   \dfrac{\vecnum{\epsilon^w}^{n+1}_{j+1}}{\rho^f } \Delta t  \matnum{A^f}^{-1}_{j,j+1}+ \dfrac{\vecnum{\phi^w}^{n+1}_{j+1} }{\rho^p } \Delta t \matnum{A^p}^{-1}_{j,j+1}\right) \dfrac{\vecnum{p_f}^*_{j+1}-\vecnum{p_f}^*_{j}}{\Delta z} \\
- \left(   \dfrac{\vecnum{\epsilon^w}^{n+1}_{j}}{\rho^f } \Delta t \matnum{A^f}^{-1}_{j}  + \dfrac{\vecnum{\phi^w}^{n+1}_{j}}{\rho^p} \Delta t \matnum{A^p}^{-1}_{j}\right) \dfrac{\vecnum{p_f}^*_{j}-\vecnum{p_f}^*_{j-1}}{\Delta z}   \\
=    \vecnum{\epsilon^w}^{n+1}_{j+1} \vecnum{w^f}^{*}_{j+1}  + \vecnum{\phi^w}^{n+1}_{j+1} \vecnum{w^p}^{*}_{j+1}  - \left(\vecnum{\epsilon^w}^{n+1}_j \vecnum{w^f}^{*}_j  + \vecnum{\phi^w}^{n+1}_j \vecnum{w^p}^{*}_j\right) 
\end{eqnarray*}

By introducing the following notations: $\vecnum{w_m^*}  =  \vecnum{\epsilon^w}^{n+1} \vecnum{w^f}^{*}  + \vecnum{\phi^w}^{n+1} \vecnum{w^p}^{*}$ and $\matnum{A_m} =  \dfrac{\vecnum{\epsilon^w}^{n+1} }{\rho^f } \Delta t  \matnum{A^f}^{-1} + \dfrac{\vecnum{\phi^w}^{n+1} }{\rho^p } \Delta t \matnum{A^p}^{-1} $, this equation can rewritten in compact form as:
\begin{eqnarray*}
     \matnum{A_m}_{j,j+1} \dfrac{\vecnum{p_f}^*_{j+1}-\vecnum{p_f}^*_{j}}{\Delta z} -  \matnum{A_m}_{j,j} \dfrac{\vecnum{p_f}^*_{j}-\vecnum{p_f}^*_{j-1}}{\Delta z}  =     \vecnum{w_m}^{*}_{j+1}  - \vecnum{w_m}^{*}_j
\end{eqnarray*}

This equation can be recasted in a matrix form $\matnum{A^{pf}}$ using the tridiagonal matrix coefficients:
$$a_N^{pf}=  \dfrac{\Delta t}{\Delta z}  \matnum{A_m}_{j,j+1} $$
$$a_S^{pf}=  \dfrac{\Delta t}{\Delta z} \matnum{A_m}_{j,j}  $$
$$a_P^{pf}= - (a_N^P+a_S^P)$$        
and the source term is written as:
$$H^{pf}=\vecnum{w_m}^{*}_{j+1}   -  \vecnum{w_m}^{*}_j$$

In terms of boundary conditions, at the top boundary (j = N-1) a homogeneous Dirichlet condition is used, the fluid pressure is set to zero:
$$\overline{p_f} = 0  \; \; ; \; \; A^{pf}_{N-1} = 1  \; \; ; \; \; H^p_{N-1}= 0. $$
At the bottom boundary (j=0), the pressure gradient is set to balance the mixture velocity flux:
$$ \dfrac{\Delta t}{\Delta z}  \matnum{A_m}_{11} \dfrac{d \overline{p_f}}{dz} = \vecnum{w_m^*}_1 \;\; ;\;\;  \matnum{A^{pf}}_{j,j+1} = 1/\Delta z \;\; ;\; \;  \matnum{A^{pf}}_{j,j} = 1/\Delta z \;\; ;\;\;    H^{pf}_j = \dfrac{ \vecnum{w_m}_1}{\Delta t \matnum{A_m}_{1,1}}$$

In a one-dimensional problem the solution of this equation is cheap and a simple double sweep algorithm can be used \cite{thomas} however for three-dimensional problems this can become very expensive as Bi-Conjugate Gradient algorithms might become necessary to resolve the algebraic system associated with the pressure equation. \\

\noindent \textbf{3. Velocity corrector step}\\

Using the newly computed pressure field $\vecnum{p_f}^*$, the velocity correction equations can be used to correct the velocity fields:
$$  \vecnum{w^f}^{**} = \vecnum{w^f}^{*} - \dfrac{\Delta t}{\rho^f} \matnum{A^f}^{-1} \dd{\vecnum{p_f}^*}{z}$$
$$  \vecnum{w^p}^{**} = \vecnum{w^p}^{*} - \dfrac{\Delta t}{\rho^p} \matnum{A^p}^{-1} \dd{\vecnum{p_f}^*}{z}$$


 \section{Implementation}

The procedure presented above has been implemented using python and is available at: {https://gricad-gitlab.univ-grenoble-alpes.fr/chauchaj/2phase1d}. The aim of this code is purely pedagogical as it is computationally inefficient. However the PISO algorithm is completely consistent with the one implemented in sedFOAM using openFOAM. 

\bibliography{/Users/chauchat/Documents/Publications/BibFiles/bibliographie_Bibdesk.bib}

\end{document}
