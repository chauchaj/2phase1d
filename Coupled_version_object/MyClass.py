import os
import numpy as np
import matplotlib.pyplot as plt
import h5py


class bedload(object):
     def __init__(self, h, tend, dz, kplot, N, h1, alpha_ini, rhop, dp, rhof, nuf, g, dt, RZ, config):
        self.dt = dt
        self.tend = tend
        self.kplot = kplot

        self.h = h
        self.h1 = h1
        ###########
        self.N = N
        self.dz = h / (float(N - 1))

        # Physical parameters
        self.dp = dp
        self.rhop = rhop
        self.rhof = rhof
        self.nuf = nuf
        self.phiSmall = 1e-6
        self.phiMax = 0.63
        self.alpha_ini = alpha_ini
        self.g = g
        self.RZ = RZ
        self.wmax = 1.1
        self.config = config

###############################################################################
################FUNCTION INITIALISATION########################################
###############################################################################  
     def initialisation(self):
         #########load results##################################################
        try:
            resultFile = h5py.File('data', "r")
            print("loaded data")
            self.zs = np.array(resultFile['zs'])
            self.zw = np.array(resultFile['zw'])
            self.phin = np.array(resultFile['phin'])
            self.wpn = np.array(resultFile['wpn'])
            self.scalarNodes = np.array(resultFile['scalarNodes'])
            self.wNodes = np.array(resultFile['wNodes'])
            self.dp = resultFile.attrs['dp']
            #self.time = resultFile.attrs['time']
            self.time = 0
            self.tp = resultFile.attrs['tps']
            self.k = 0
            self.N = len(self.zs)
        #########initialisation if no results to load##########################
        except IOError:
            print("No results found")

            # Mesh definition
            # scalar points
            self.zs=np.linspace(0,self.h,num=self.N)
            self.zs=self.zs/self.h
            # Velocity points
            self.zw=np.linspace(-self.dz/2,self.h+self.dz/2,num=self.N+1)
            self.zw=self.zw/self.h
            
            self.zw[0]=0
            self.zw[self.N]=1
            
            # Internal nodes list
            self.wNodes = range(self.N-1)
            self.wNodes = [x+1 for x in self.wNodes]
            
            self.scalarNodes = range(self.N-2)
            self.scalarNodes = [x+1 for x in self.scalarNodes]
            
            self.time = 0
            self.k = 0
            
            self.phin = np.zeros(self.N)
            self.wpn = np.zeros(self.N + 1)
            self.wfn = np.zeros(self.N + 1)
            ###############################################################################
            #
            # Physical parameters (Pham Van Bang et al. 2006)
            #
            ###############################################################################
            # Stokes response time and settling velocity
            self.tp=self.rhop*self.dp**2/(18.*self.nuf*self.rhof)
            self.wstokes = (self.rhop-self.rhof)*self.g*self.dp**2/(18.*self.rhof*self.nuf)
            
            print('tpStokes=',self.tp,'Wstokes=',self.wstokes)
            
            ###############################################################################
            #
            # Initial condition
            #
            ###############################################################################
            self.phin = self.alpha_ini*0.5*(1+np.tanh((self.h1/self.h-self.zs)*300))
            self.wpn[:] = -self.wstokes/10.
            self.wpn[0] = 0
            
###############################################################################
################FUNCTION TO CREATE MISSING ARGUMENTS###########################
###############################################################################
     def  addArgument(self):
        # Volume fraction
        self.phinp = np.zeros(self.N)
        self.phiw = np.zeros(self.N+1)
        self.phiDrag = np.zeros(self.N+1)
        
        # Pressure 
        self.pfnp = np.zeros(self.N)
        self.pp   = np.zeros(self.N)
        
        self.dpfdz = np.zeros(self.N+1)
        self.dppdz = np.zeros(self.N+1)
        
        # Velocities
        self.wpst = np.zeros(self.N+1)
        self.wfst = np.zeros(self.N+1)
        self.wpnp = np.zeros(self.N+1)
        self.wfnp = np.zeros(self.N+1)
        
        self.wn = np.zeros(2*(self.N+1))
        self.wst = np.zeros(2*(self.N+1))
        self.wnp = np.zeros(2*(self.N+1))
        
        self.A = np.zeros((2*(self.N+1),2*(self.N+1)))
        self.H = np.zeros(2*(self.N+1))
        
        # Matrices and RHS
        # Volume fraction
        self.Aa = np.zeros((self.N,self.N))
        self.Ha = np.zeros(self.N)
        
        # Velocities
        self.Ap = np.zeros((self.N+1,self.N+1))
        self.Af = np.zeros((self.N+1,self.N+1))
        self.Apinv = np.zeros((self.N+1,self.N+1))
        self.Afinv = np.zeros((self.N+1,self.N+1))
        self.Ap_f = np.zeros((self.N+1,self.N+1))
        self.Af_p = np.zeros((self.N+1,self.N+1))
        self.Hp = np.zeros(self.N+1)
        self.Hf = np.zeros(self.N+1)
        
        # Pressure
        self.Apf = np.zeros((self.N,self.N))
        self.Hpf = np.zeros(self.N)

        self.div  = np.zeros((self.N,self.N+1))
        self.grad = np.zeros((self.N+1,self.N))
        
        self.Iwp = np.block([np.identity(self.N+1),np.zeros((self.N+1,self.N+1))])
        self.Iwf = np.block([np.zeros((self.N+1,self.N+1)),np.identity(self.N+1)])
###############################################################################
################FUNCTION TO CREATE OPERATOR####################################
###############################################################################
     def operator(self):
        # gradient operator
        for j in self.wNodes:
            self.grad[j,j-1] = - 1/self.dz
            self.grad[j,j  ] =   1/self.dz   
        # Bottom boundary condition
        j=0
        self.grad[j,j  ] = -1/self.dz
        self.grad[j,j+1] =  1/self.dz
        
        # Top boundary condition
        j=self.N
        self.grad[j,j-2] = -1/self.dz
        self.grad[j,j-1] =  1/self.dz
        
        # Divergence operator
        for j in self.wNodes:
            self.div[j][j  ] = - 1/self.dz
            self.div[j][j+1] =   1/self.dz   
        # Bottom boundary condition
        j=0
        self.div[j][j  ] = -1/(0.5*self.dz)
        self.div[j][j+1] =  1/(0.5*self.dz)
        
        #matrice d'interpolation
        self.Iphi = np.zeros((self.N+1,self.N))
        self.IphiDrag = np.zeros((self.N+1,self.N))
        for j in self.wNodes:
            self.Iphi[j][j-1] = 0.5
            self.Iphi[j][j  ] = 0.5 
            self.IphiDrag[j][j-1] = 1.
        j=0
        self.Iphi[j,j] = 1.
        self.IphiDrag[j,j] = 1.
        j=self.N
        self.Iphi[j,j-1] = 1.
        self.IphiDrag[j,j-1] = 1.
        
###############################################################################
################FUNCTION TO INTERPOLATE####################################
###############################################################################
     def interpolation(self, Xw, I, X):
         Xw = I.dot(X)

###############################################################################
################FUNCTION FOR MASS CONSERVATION#################################
###############################################################################
     def massConservation(self):
         ########################################################
        #
        #        Mass conservation equation
        #
        ########################################################
        for j in self.scalarNodes:
            FS = self.dt *  self.wpn[j  ] / self.dz
            FN = self.dt *  self.wpn[j+1] / self.dz
            #
            self.Aa[j][j-1] = - max( FS,0)
            self.Aa[j][j+1] = - max(-FN,0)
            self.Aa[j][j] = 1e0 -self.Aa[j][j+1] -self.Aa[j][j-1] + FN - FS 
            self.Ha[j] = self.phin[j]

        # Bottom boundary condition: Neumann
        j=0
        FN = self.dt *  self.wpn[j+1] / (0.5*self.dz)
        #
        self.Aa[j][j+1] = - max(-FN,0)
        self.Aa[j][j] = 1e0 -self.Aa[j][j+1] + FN 
        self.Ha[j] = self.phin[j]
            
        # Top boundary condition: Dirichlet
        self.Aa[self.N-1][self.N-1]=1e0
        self.Ha[self.N-1]=0e0

        # Mass conservation resolution
        self.phinp = np.linalg.solve(self.Aa, self.Ha)
    
###############################################################################
############################SOLID PRESSURE ####################################
###############################################################################
     def solidPressure(self):
        for j in range(self.N):
            self.pp[j] = 5e-2 * (max(self.phinp[j] - 0.57, 0)) ** 3 \
                         / (max(0.625 - self.phinp[j], self.phiSmall)) ** 5
                         
        # Compute the vertical gradient of particle pressure
        self.dppdz = self.grad.dot(self.pp)
        
###############################################################################
########################ADVECTION SCHEMES######################################
###############################################################################
     def advection_schemes(self):
         for j in self.wNodes:
             # Solid phase matrix  
            if j==1:
                FS = self.dt *  self.wpn[j-1] / self.dz
            else:
                 FS = self.dt *  0.5*(self.wpn[j  ] + self.wpn[j-1]) / self.dz
            if j==self.N-1:
                FN = self.dt *  self.wpn[j+1] / self.dz
            else:
                FN = self.dt *  0.5*( self.wpn[j+1]+ self.wpn[j  ]) / self.dz
            #
            self.Ap[j][j-1] = - max( FS,0) 
            self.Ap[j][j+1] = - max(-FN,0)
            self.Ap[j][j] = 1e0 - self.Ap[j][j-1] - self.Ap[j][j+1] + FN - FS \
                       + self.dt/self.tp*(1-self.phiDrag[j])**self.RZ
            self.Hp[j] = self.wpn[j] - self.dt*(1.-self.rhof/self.rhop)*self.g \
                    -self.dt*self.dppdz[j]/(self.rhop*(self.phiw[j]+self.phiSmall)) 
            # -dt*(1.-rhof/rhop)*g
            
            # coupling Matrix for the solid phase
            self.Ap_f[j][j]= - self.dt/self.tp*(1-self.phiDrag[j])**self.RZ
            
            # fluid phase matrix
            if j==1:
                FS = self.dt *  self.wfn[j-1] / self.dz
            else:
                 FS = self.dt * 0.5*(self.wfn[j  ]+ self.wfn[j-1]) / self.dz
            if j==self.N-1:
                FN = self.dt *  self.wfn[j+1] / self.dz
            else:
                FN = self.dt *  0.5*(self.wfn[j+1] + self.wfn[j  ]) / self.dz
            #
            self.Af[j][j-1] = - max( FS,0) 
            self.Af[j][j+1] = - max(-FN,0)
            self.Af[j][j] = 1e0 - self.Af[j][j-1] - self.Af[j][j+1] + FN - FS \
                       + self.dt*self.rhop*self.phiDrag[j]/(self.rhof*(1.-self.phiDrag[j])*self.tp)*\
                       (1-self.phiDrag[j])**self.RZ 
            self.Hf[j] = self.wfn[j] #- dt*g 
            
            # coupling Matrix for the fluid phase
            self.Af_p[j][j] = - self.dt*self.rhop*self.phiDrag[j] \
                           /(self.rhof*(1.-self.phiDrag[j])*self.tp)*(1-self.phiDrag[j])**self.RZ
            
         # Bottom boundary condition: impermeable wall
         #Particle phase
         j = 0
         self.Ap[0,0]   = 1e0
         self.Ap_f[0,:] = 0
         self.Hp[0]     = 0e0
         # Fluid phase
         self.Af[0,0]   = 1e0
         self.Af_p[0,:] = 0
         self.Hf[0]     = 0e0
         
         # Top boundary condition: momentum equation
         j=self.N
         self.Ap[j,j-1] = -1
         self.Ap[j,j  ] =  1
         self.Af_p[j,:] = 0
 
         self.Hp[j] = 0

         self.Af[j,j-1] = -1
         self.Af[j,j  ] =  1
         self.Ap_f[j,:] = 0
 
         self.Hf[j] = 0

###############################################################################
########################POISSON PRESSURE EQUATION##############################
###############################################################################
     def poisson_correction(self, Am, wm, AoneOverRho):
         self.Apf = -self.dz*self.div.dot(Am.dot(self.grad))
         self.Hpf = -self.dz*self.div.dot(wm)/self.dt
                
         # Bottom boundary condition: fixedFluxPressure
         j = 0
         self.Apf[j,:] = 0
         self.Apf[j,j+1] = 1./self.dz
         self.Apf[j,j  ] = -1./self.dz 
         self.Hpf[j] =  wm[1]/(self.dt*Am[1,1])  
         
         # Top boundary condition: Dirichlet pf=0
         j = self.N-1
         self.Apf[j,:] = 0
         self.Apf[j,j] = 1
         self.Hpf[j] = 0
         
         # resolve poisson equation matrix
         self.pfnp = np.linalg.solve(self.Apf, self.Hpf)
         
         # Compute fluid phase pressure gradient
         self.dpfdz = self.grad.dot(self.pfnp)
         
         ########################################################
         #
         # Velocity correction
         #
         ########################################################
         self.wnp = self.wst - self.dt*(self.Ainv.dot(AoneOverRho)).dot(self.dpfdz)
         
         self.wpnp = self.Iwp.dot(self.wnp)
         self.wfnp = self.Iwf.dot(self.wnp)
         
         # Bottom boundary condition: impermeable wall
         self.wpnp[0]=0e0
         self.wfnp[0]=0e0
         self.wpnp[self.N]=self.wpnp[self.N-1]
         self.wfnp[self.N]=self.wfnp[self.N-1]
         
                
            

        
